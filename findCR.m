%----------- Determining Compression Ratio

orig_info=dir('images/original_img_half_grey.png');
comp_info=dir('compressed_images/dct.png');
huffman1_info=dir('compressed_images/huffman_image.mat');
huffman2_info=dir('compressed_images/huffman_image.m');
dct_huffman1_info=dir('compressed_images/dct_huffman_image.mat');
dct_huffman2_info=dir('compressed_images/dct_huffman_image.m');

orig_size=orig_info.bytes;
DCT_size=comp_info.bytes;
Huff_size=huffman1_info.bytes + huffman2_info.bytes;
DCT_Huff_size=dct_huffman1_info.bytes + dct_huffman2_info.bytes;

DCT_CR=double(orig_size)/double(DCT_size);
Huff_CR=double(orig_size)/double(Huff_size);
DCT_Huff_CR=double(orig_size)/double(DCT_Huff_size);

fprintf("The compression ratio of DCT is: %.4f\n", DCT_CR);
fprintf("The compression ratio of Huffman encoding is: %.4f\n", Huff_CR);
fprintf("The compression ratio of DCT + Huffman encoding is: %.4f\n" ,DCT_Huff_CR);