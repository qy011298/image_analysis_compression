original_image=imread('images/original_img_half_grey.png');
dct88_98=imread('compressed_images/dct88_98.png');
dct1818_99=imread('compressed_images/dct1818_99.png');

mse1=immse(original_image,dct88_98);
mse2=immse(original_image,dct1818_99);

title1=sprintf("Non compressed image");
title2=sprintf("DCT 8x8 98.4 MSE: %0.2f", mse1);
title3=sprintf("DCT 18x18 99.7 MSE: %0.2f", mse2);

figure('Name','DCT Transformations')
subplot(1,3,1), imshow(original_image), title(title1);
subplot(1,3,2), imshow(dct88_98), title(title2);
subplot(1,3,3), imshow(dct1818_99), title(title3);
